from numbers import Number

# import rip


def add_DPH(prise: Number) -> Number:
    """
    Spočítá cenu s dph 21%.
    """
    return prise * 1.21


def eco_vat(prise: Number) -> Number:
    """
    Spočítá cenu s dph 21%.
    """
    return prise * 1.01
