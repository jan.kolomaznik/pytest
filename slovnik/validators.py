# %% Hard valitdator
def validatorForHard(world: str, minLength=6) -> str | None:
    """Vybírá ze slovníku těžká slova (/H)
    """
    
# %% main block
if __name__ == '__main__':
    print(validatorForHard('abakus/H'))
    print(validatorForHard('abbéům'))
    print(validatorForHard('aktivizovaný/YKRN'))
# %%
