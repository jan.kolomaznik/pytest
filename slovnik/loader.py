# %% Funkce nacti
def load_slovnik(path_to_dic: str,
                 encoding='iso-8859-2',
                 validator: callable = lambda w: w
    ) -> list[str]:
    """Vytvoří seznam slov ze souboru.

    Args:
        path_to_dic (str): cesta k souboru slovniku
        validator: upravi slovo ve slovniku, nebo vraci None, když se má vyřadit
    Returns:
        list[str]: seznam slov
    """
    try:
        with open(path_to_dic, mode='r', encoding=encoding) as dic_file:
            return dic_file.readlines()
    except FileNotFoundError as exc:
        raise ValueError(f"Dic file {path_to_dic!r} not exists.")

# %% main block
if __name__ == '__main__': 
    slovnik = load_slovnik('static/cs_CZ.dic')
    print(slovnik[:10])
# %%
