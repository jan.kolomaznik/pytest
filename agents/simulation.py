from itertools import count

def simulation(stav: Stav, agent: callable):
    i = count()
    memory = {}
    while stav.contracts:
        city = agent(stav, memory)
        stav = stav.move(city)
        print(f"{next(i)}: {stav}, {memory}")

