
from pexeso.pexeso import Pexeso

class TestPexeso:

    def test_aktivni_hrac(mocker):
        # Arrage
        stav = Pexeso([])
        # Act
        hrac = stav.aktivni_hrac
        # Assert
        assert hrac is not None

    def test_odhal_kartu(self):
        # Arrage
        stav = Pexeso()
        # Act
        karta = stav.odhal_kartu('A', 2)
        # Assert
        assert karta is not None

    def test_odhal_kartu__na_orazdnem_miste(self):
        """Resim pripad kdy chci odhalit kartu, a miste kde jiz byla odebrana"""
        # Arrage
        stav = Pexeso()
        # Act + assert
        with pytest.raises(ValueError):
            stav.odhal_kartu('A', 23)

    def test_prirad_karty_aktivnimu_hraci(self):
        # Arrage
        stav = Pexeso()
        karta_1 = stav.odhal_kartu('A', 1)
        karta_2 = stav.odhal_kartu('A', 2)
        # Act
        novy_stav = stav.prirad_karty_aktivnimu_hraci(karta_1, karta_2)
        # Assert 
        # hrci byli prirazeny karty
        assert karta_1 in stav.aktivni_hrac.karty
        assert karta_2 in stav.aktivni_hrac.karty
        # ze aktibni hrac zustal stale aktivni
        assert novy_stav.aktivni_hrac == stav.aktivni_hrac
        # ze z herniho pole byli odbrany zadane karty
        with pytest.raises(ValueError):
            novy_stav.odhal_kartu('A', 1)
        with pytest.raises(ValueError):
            novy_stav.odhal_kartu('A', 2)