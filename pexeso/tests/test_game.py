import pytest

from pexeso.game import PexesoGame
from pexeso import Pexeso

from unittest.mock import Mock

class TestPexesoGame:

    def test_turn_interation(self, mocker):
        # Arrage
        stav_spy = mocker.spy(Pexeso, 'odhal_kartu')
        prvni_pole = ('A',1)
        druhe_pole = ('D',3)
        game = PexesoGame()
        # Act
        stav = game.turn(prvni_pole, druhe_pole)
        # Assert
        assert stav_spy.call_count == 2

    def test_turn_unit(self):
        # Arrage
        stav = Mock()
        prvni_pole = ('A',1)
        druhe_pole = ('D',3)
        game = PexesoGame(stav)
        # Act
        result = game.turn(prvni_pole, druhe_pole)
        # Assert
        # assert stav.odhal_kartu.assert_called_with('A',1)
        assert stav.odhal_kartu.assert_called_with('D',3)