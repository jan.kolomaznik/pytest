from .pexeso import Pexeso

class PexesoGame:
    """Tato trida bude mit za ospovednost pruhe jedne partie."""
    
    def __init__(self, stav = None):
        self.__stav = stav or Pexeso([])

    @property
    def stav(self):
        return self.__stav

    def turn(self, prvni_pole, druhe_pole):
        """Prevede jeden tah ve hre.
           1. Aktivni hrac vybere prvni neotocenou kartu
           2. Aktivni hrc vybere druhou neotocenou katru
           3. Pokud karty patri k sobe, 
              - odeberou z herniho pole a priradi se k hraci
              - hrac pokracuje dalim tahem
           4. Pokud neuhodl svojici, pak jeho tah konci a hraje salsi hrac.
        """
        stav = self.__stav
        karta_1 = stav.odhal_kartu(*prvni_pole)
        karta_2 = stav.odhal_kartu(*druhe_pole)
        if karta_1 == karta_2:
            self.__stav = stav.prirad_karty_aktivnimu_hraci(karta_1, karta_2)
        else:
            self.__stav = stav.dalsi_hrac()
        
        

