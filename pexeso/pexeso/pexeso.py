
class Pexeso:
    """Trida uchovajisi aktualni stav hry. 
       Tedy karticky ktere jsou jeste neodebrane a seznamy kraticek vsech hracu.
    """

    def __init__(self, hraci):
        self.__hraci = hraci

    @property
    def aktivni_hrac(self) -> str:
        """Urcuje, ktery z usatniku hry (hrac) ve hre je prave na tahu"""

    def odhal_kartu(self, radek: str, sloupec: int):
        """Pouzi pro ziskani 'karty' na zadanych souradnicich."""

    def prirad_karty_aktivnimu_hraci(self, karta_1, karta_2) -> 'Pexeso':
        """Vraci novy stav jsou kary odebrany z herniho pole a prirazeny prave aktivnimu hraci."""
        