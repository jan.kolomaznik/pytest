roads = [
  "Alice's House-Bob's House",
  "Alice's House-Cabin",
  "Alice's House-Post Office",
  "Bob's House-Town Hall",
  "Daria's House-Ernie's House",
  "Daria's House-Town Hall",
  "Ernie's House-Grete's House",
  "Grete's House-Farm",
  "Grete's House-Shop",
  "Marketplace-Farm",
  "Marketplace-Post Office",
  "Marketplace-Shop",
  "Marketplace-Town Hall",
  "Shop-Town Hall",
]

from collections.abc import Iterable

def loadCity(roads: Iterable[str] = roads) -> dict:
    city = {} #{ nazev:list[mista]}
    for a,b in map(lambda r: r.split('-'), roads):
        #a,b = road.split('-')
        city[a] = city[a] | {b} if a in city else {b}
        city[b] = city[b] | {a} if b in city else {a}
    return city