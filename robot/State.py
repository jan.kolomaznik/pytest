from .Balik import Balik

class State:
    city = {}
    
    def __init__(self, robot, baliky):
        self.__robot = robot
        self.__baliky = baliky
        
    @property
    def robot(self):
        return self.__robot
    
    @property
    def baliky(self):
        return self.__baliky
    
    def presunRobota(self, nove_misto:str) -> 'State':
        """Vytvori novy stav sveta ve ktrem je robot presunut na nove misto 
        (pokud je nove misto sousedici a aktualnim mistem) a splu s robote 
        presune vsechny baliky a doruci dorucitelne.
        """
        assert self.__robot != nove_misto, 'Přesun na stejné místo jako původní'
        assert State.city, 'Nenastavena statická mapa mesta pro State.'
        
        if nove_misto not in State.city[self.__robot]:
            return self
        
        baliky_presunute = map(
            lambda b: b if b.misto != self.__robot else Balik(misto=nove_misto, adresa=b.adresa),
            self.__baliky
        )
        baliky_nedorucene = filter(
            lambda b: b.adresa != b.misto,
            baliky_presunute
        )
        return State(nove_misto, list(baliky_nedorucene))
    
    def presunRobotaMutable(self, nove_misto:str):
        """Vytvori novy stav sveta ve ktrem je robot presunut na nove misto 
        (pokud je nove misto sousedici a aktualnim mistem) a splu s robote 
        presune vsechny baliky a doruci dorucitelne.
        """
        if nove_misto not in State.city[self.__robot]:
            raise ValueError("Nedosazitelne misto")
        old_place = self.__robot
        self.__robot == nove_misto
        
        baliky_presunute = map(
            lambda b: b if b.misto != old_place else Balik(misto=nove_misto, adresa=b.adresa),
            self.__baliky
        )
        baliky_nedorucene = filter(
            lambda b: b.adresa != b.misto,
            baliky_presunute
        )
        self.__baliky = list(baliky_nedorucene)

    
    
    
    def __repr__(self):
        return f"State(robot={self.__robot}, baliky={self.__baliky})"
    
    def __len__(self):
        return len(self.__baliky)