import random

def randomRobot(stav, city) -> str:
    destination = random.choice(list(city[stav.robot]))
    assert stav.robot in city[destination], f"Detect one way road from {stav.robot} to {destination}."
    return destination