import pytest
import requests

def test_get_root():
    # Arrange
    # Act
    response = requests.get("http://localhost:8000/")
    # Assert
    assert response.status_code == 200

def test_get_items_by_id():
    # Arrange
    item_id = 123
    query = "Dotaz"
    # Act
    response = requests.get(f"http://localhost:8000/items/{item_id}?q={query}")
    # Assert
    assert response.status_code == 200

    # dle meho nazoru zbytečne detailni tesovani
    json = response.json()
    assert json["item_id"] == item_id
    assert json["q"] == query