from random import choice
from unidecode import unidecode

def select_word(dictionary: list[str]) -> str:
    """Vybere vhodne slovo pro hru sibenice ze seznamu slov"""
    # try:
    #     return choice(dictionary)
    # except IndexError:
    #     raise ValueError("Can't select world from empty dictionary.")
    return choice(dictionary) if dictionary else None


def word_validator(world: str) -> None | str:
    """Rozhoduje zda slovo pouzit nebo ne. 
    Pokud je pouzitelné vrátí jej v unifikovane forme jinak vraci None"""
    try:
        slovo, sufix = world.split(sep="/", maxsplit=1)
        if "H" not in sufix or len(slovo) < 4:
            return None
        slovo_bez_diakritiky = unidecode(slovo)
        return slovo_bez_diakritiky.lower()
    except ValueError:
        return None

def load_dictionary(path_dictionory: str) -> list[str]:
    """Funkce pro načtení souboru ve formatu dic jako slovniku"""
    with open(path_dictionory, mode="r", encoding="utf-8") as dic_file:
        return dic_file.readlines()
  
from operator import truth

class Sibenice:
    
    def __init__(self, slovnik: list[str]):
        valid_word = list(filter(truth, map(word_validator, slovnik)))
        self.word = select_word(valid_word)