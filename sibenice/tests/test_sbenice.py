import pytest

import sys
sys.path.append('src')

import random
from slovnik import *
import slovnik

def test_select_word():
    slovnik = ["Brno", "Adam", "Python"]
    # Act
    result = select_word(slovnik)
    # Assert
    assert result is not None
    assert result in slovnik

def test_select_word__shuld_fail_on_empty_list():
    # Arrange
    empty_slovnik = []
    # Act + Assert
    with pytest.raises(ValueError, match=r".*empty dictionary.*"):
        select_word(empty_slovnik)
    
@pytest.mark.parametrize("word, uniform", [
    ("abcdef/H", "abcdef"),
    ("Czech/H", "czech"),
    ("ďábelské/H", "dabelske")
    # ("Příliš žluťoučký kůň úpěl ďábelské ódy/H", "prilis zlutoucky kun upel dabelske ody"),
])
def test_word_validator__world_is_valid(word: str, uniform: str):
    # Act
    result = word_validator(word)
    # Assert
    assert result == uniform
    
@pytest.mark.parametrize("word", [
    "abcdef",
    "Czech/F",
    "ódy/H",
])
def test_word_validator__world_is_invalid(word: str):
    # Act
    result = word_validator(word)
    # Assert
    assert result is None

@pytest.mark.skip    
def test_load_dictionary(tmp_path):
    dic_file = tmp_path / "dictionary.dic"
    dic_file.write_text("""3
Honza/H
Jirka
Evička
""", encoding="utf-8")
    # Act
    result = load_dictionary(dic_file)
    # Assert
    assert result == ["Honza\H", "Jirka", "Evička"]
   
def test_load_dictionary_not_found_error():
    # Act
    with pytest.raises(FileNotFoundError):
        load_dictionary("not_exists.dic")

@pytest.mark.skip      
class TestSibenice:
    
    def test_init(self, monkeypatch):
        dictionary = ["Honza", "Jirka", "Eva"]
        monkeypatch.setattr(slovnik, 'word_validator', lambda w: w)
        
        def choice_first(collection):
            assert collection == dictionary
            return collection[0]
        monkeypatch.setattr(slovnik, 'select_word', choice_first)
        # Act
        game = Sibenice(dictionary)
        # Assert
        assert game.word == "Honza"